# GitLab Pa11y CI

## About GitLab Pa11y CI

GitLab Pa11y CI is a container image designed to perform web accessibility (a11y) testing using [Pa11y CI](https://github.com/pa11y/pa11y-ci) in a GitLab CI job. The image is based on the [Docker Puppeteer](https://gitlab.com/gitlab-ci-utils/docker-puppeteer) project with the proper [Puppeteer](https://github.com/GoogleChrome/puppeteer) configuration.

> As of v10.0.0 this project has moved from `pa11y` to fork [`@aarongoldenthal/pa11y`](https://github.com/aarongoldenthal/pa11y) and from `pa11y-ci` to fork [`@aarongoldenthal/pa11y-ci`](https://github.com/aarongoldenthal/pa11y-ci) due to lack of sustaining of the upstream projects.

## Configuration

GitLab Pa11y CI is intended to be run as a GitLab CI job with a configuration file for Pa11y CI, as shown in the examples below.

### Pa11y CI configuration

By default, Pa11y CI looks for a JSON-formatted configuration file named
`.pa11yci`, `.pa11yci.json`, or `.pa11yci.js` in the current working directory.
An example is shown below that runs Pa11y CI against two specific URLs,
checking for both errors and warnings against the WCAG 2.x-AA standard (the
default), and saves a screenshot for one URL.

```json
{
  "defaults": {
    "includeWarnings": true
  },
  "urls": [
    "https://www.w3.org/WAI/demos/bad/before/home.html",
    {
      "url": "https://www.w3.org/WAI/demos/bad/before/news.html",
      "screenCapture": "www-w3-org-WAI-demos-bad-before-news.png"
    }
  ]
}
```

Complete details on Pa11y CI configuration options can be found at
<https://github.com/pa11y/pa11y-ci#configuration>.

If not using a configuration file, or using a configuration file that doesn't
include URLs, command line arguments in the CI script should identify the URLs
or `sitemap.xml` file to be analyzed.

#### The dreaded `No usable sandbox!` error

When Chromium is launched by a non-root user, which this container image uses,
it tries to create a
[sandbox](https://chromium.googlesource.com/chromium/src/+/refs/heads/main/docs/design/sandbox.md)
for process isolation. In some cases this can't be done, and it fails with a
message like `Error: Failed to launch chrome! ... No usable sandbox!`.
Typically this is due to the
[seccomp profile](https://docs.docker.com/engine/security/seccomp/) that the
container runtime uses to limit calls that can be made from the container to
the host kernel.

In these cases either the container needs to be run with an updated seccomp
profile with the appropriate permissions, or Chromium needs to be launched with
the `--no-sandbox` argument so it does not try to create the sandbox.
Additional background, compatible seccomp profiles, and details on how to
specify the profile can be found
[here](https://stackoverflow.com/questions/50662388/running-headless-chrome-puppeteer-with-no-sandbox)
and [here](https://playwright.dev/docs/docker/#run-the-image).

In some cases it may not be possible or practical to specify the seccomp
profile when launching the container, for example when using gitlab.com
runners, so the `--no-sandbox` argument must be used if there is an issue
creating a sandbox (although generally this isn't an issue on gitlab.com shared
runners with this container image). Also, if running in a container as root,
the `--no-sandbox` argument is always required.

**Note: running with the `--no-sandbox` argument, as the name suggests, disables
the [browser sandbox](https://chromium.googlesource.com/chromium/src/+/HEAD/docs/design/sandbox.md),
a security feature of the browser. This should be done with caution and only
loading known and understood code.**

To have Puppeteer pass the `--no-sandbox` argument to Chromium it must be
specified in the `chromeLaunchConfig` configuration option as detailed below.

```json
{
  "defaults": {
    "chromeLaunchConfig": {
      "args": ["--no-sandbox"]
    }
  }
}
```

### GitLab CI configuration

The following is an example job from a `.gitlab-ci.yml` file to use this image
to run Pa11y CI in a GitLab CI job. Note that `pa11y` and `pa11y-ci` are added
to the `PATH` so they can be run directly.

```yml
pa11y-ci:
  image: registry.gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci:latest
  stage: test
  script:
    - pa11y-ci --reporter=json > pa11y-ci-results.json
  artifacts:
    # Make sure artifacts are always saved, otherwise they will not be on failure.
    when: always
    paths:
      - pa11y-ci-results.json
```

This example assumes that a Pa11y CI configuration file with a default name as
noted previously is in the project root that specifies the URLs to be scanned
and outputs the JSON-formatted file `pa11y-ci-results.json` which is saved as
an artifact.

Command line arguments can be used to specify an alternate configuration file,
specific URLs to scan, using a `sitemap.xml` file to identify URLs to scan,
etc. Complete details on available command line options can be found at
<https://github.com/pa11y/pa11y-ci#usage>.

### Pa11y and Pa11y CI reporters

The container images includes the following Pa11y and Pa11y CI reporters
(beyond those included with the tools themselves):

- `pa11y-ci-reporter-html` is a Pa11y CI reporter that generates HTML reports
  for Pa11y CI results. Full details on use of this reporter can be found
  [here](https://www.npmjs.com/package/pa11y-ci-reporter-html).
- `pa11y-reporter-html-plus` is a Pa11y reporter that generates the
  page-specific HTML reports for `pa11y-ci-reporter-html`, but is also
  available as a standalone Pa11y reporter. Full details on use of this
  reporter can be found
  [here](https://www.npmjs.com/package/pa11y-reporter-html-plus).
- `pa11y-ci-reporter-cli-summary` is a Pa11y CI reporter that outputs a summary
  of Pa11y CI results to stdout - only the URL and a count of the
  errors/warning/notices for each page. Full details on use of this reporter
  can be found
  [here](https://www.npmjs.com/package/pa11y-ci-reporter-cli-summary).

### Serving static content

The container image also includes two packages to simplify serving static
content for testing:

- [`serve`](https://www.npmjs.com/package/serve): A web server with a CLI
  interface for serving static content. See the package details for full
  configuration options (CLI argument and configuration file).
- [`wait-on`](https://www.npmjs.com/package/wait-on): A CLI utility to wait
  for a specific resource to become available. In this case it waits until
  `serve` is initialized and serving content.

The following examples shows how to start a static web server for Pa11y/Pa11y
CI analysis. Note that `serve` and `wait-on` are added to the `PATH` so they
can be run directly.

```yml
pa11y-ci:
  image: registry.gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci:latest
  stage: test
  variables:
    PATH_PREFIX: https://some-domain.com/
  before_script:
    # Start static server and wait until running
    - serve ./public/ > /dev/null 2>&1 & wait-on http://localhost:3000/
  script:
    - >
      pa11y-ci
      --sitemap http://localhost:3000/sitemap.xml
      --sitemap-find $PATH_PREFIX
      --sitemap-replace
      http://localhost:3000/
```

This serves content from the `./public/` directory, with the server output
piped to the null device to avoid output to the job log. The `wait-on` command
waits until the server is available (port 3000 is the default for `serve`).

This example also shows how to analyze URLs with Pa11y CI using a sitemap,
replacing the published URL prefix (<https://some-domain.com/>) with the static
server address (<http://localhost:3000/>).

## GitLab Pa11y CI container images

All available container image tags can be found in the
`gitlab-ci-utils/gitlab-pa11y-ci` repository at
<https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/container_registry>. Details
on each release can be found on the
[Releases](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/releases) page.

**Note:** any images in the `gitlab-ci-utils/gitlab-pa11y-ci/tmp` repository
are temporary images used during the build process and may be deleted at any
point.
