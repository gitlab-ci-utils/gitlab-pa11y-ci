FROM registry.gitlab.com/gitlab-ci-utils/container-images/puppeteer:node-22.11.0-bookworm-slim@sha256:9e503c64a1ceb8c714e011478e44e14cb6211b3b972569fb448ada65f0c78f70

ENV PUPPETEER_CACHE_DIR=/home/pptruser/.cache/puppeteer

# Image uses a lesser privileged account, but need root to
# install Puppeteer with npm dependencies
USER root

# Pa11y-ci is installed locally because permission issues were encountered
# with the Puppeteer/Chrome installation when installed globally.
WORKDIR /gitlab-pa11y-ci
COPY package*.json ./
RUN npm ci --omit=dev

# Update path so executable can be run from anywhere
ENV PATH="/gitlab-pa11y-ci/node_modules/.bin:${PATH}"

USER pptruser

LABEL org.opencontainers.image.licenses="LGPL-3.0-only"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci"
LABEL org.opencontainers.image.title="GitLab Pa11y CI"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci"

CMD [ "pa11y-ci" ]
