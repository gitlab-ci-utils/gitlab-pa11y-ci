# Changelog

## 11.3.4 (2025-03-02)

### Fixes

- Updated to [`@aarongoldenthal/pa11y@8.5.4`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.4.4`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to `puppeteer@24.3.0`, which includes fixes and security updates
    to Chrome 133 and Firefox 135.

## 11.3.3 (2025-02-09)

### Fixes

- Updated to [`@aarongoldenthal/pa11y@8.5.3`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.4.3`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to `puppeteer@24.2.0`, which includes updating to Chrome 133 and
    Firefox 135.

## 11.3.2 (2025-01-26)

### Fixes

- Updated to [`@aarongoldenthal/pa11y@8.5.2`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.4.2`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to `puppeteer@24.1.1`, which includes fixes and security updates
    to Chrome 132 and Firefox 134.

## 11.3.1 (2025-01-20)

### Fixes

- Updated to [`@aarongoldenthal/pa11y@8.5.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.4.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@24.1.0, which includes updating to Chrome 132.

## 11.3.0 (2025-01-10)

### Changes

- Updated to [`@aarongoldenthal/pa11y@8.5.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.4.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to `puppeteer@24.0.0`, which includes security updates to Chrome
    131 and updates to Firefox 134. It also removes the deprecated launch
    option interface, which should not be breaking as the new interface only
    adds capabilities. See the `puppeteer`
    [documentation](https://pptr.dev/api/puppeteer.launchoptions) for complete
    details.

### Fixes

- Updated to latest dependencies (`wait-on@8.0.2`).

## 11.2.4 (2024-12-23)

### Fixed

- Updated to [`@aarongoldenthal/pa11y@8.4.4`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.3.4`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.11.1, which includes security updates to Chrome 131.

## 11.2.3 (2024-12-15)

### Fixed

- Updated to [`@aarongoldenthal/pa11y@8.4.3`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.3.3`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.10.4, which includes security updates to Chrome 131
    and Firefox 133.

## 11.2.2 (2024-12-08)

### Fixed

- Updated to [`@aarongoldenthal/pa11y@8.4.2`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.3.2`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.10.1, which includes security updates to Chrome 131
    and updating to Firefox 133.

## 11.2.1 (2024-11-24)

### Fixed

- Updated to [`@aarongoldenthal/pa11y@8.4.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.3.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.9.0, which includes security updates to Chrome 131.

## 11.2.0 (2024-11-17)

### Changed

- Updated to [`@aarongoldenthal/pa11y@8.4.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.3.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.8.0, which includes updating to Chrome 131.

### Fixed

- Refreshed lockfile to resolve
  [CVE-2024-21538](https://nvd.nist.gov/vuln/detail/CVE-2024-21538) /
  [GHSA-3xgq-45jj-v275](https://osv.dev/vulnerability/GHSA-3xgq-45jj-v275).
- Updated to `pa11y-ci-reporter-cli-summary@4.0.1`.

## 11.1.0 (2024-11-10)

### Changed

- Updated to [`@aarongoldenthal/pa11y@8.3.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.2.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.7.1, which includes security updates to Chrome 130
    and updating to Firefox 132.

## 11.0.0 (2024-11-04)

### Changed

- BREAKING: Updated base image to Node 22, which is the active LTS as of
  2024-10-29 per the
  [Node releases schedule](https://github.com/nodejs/Release?tab=readme-ov-file#nodejs-release-working-group).
  (#59)
- Updated to [`@aarongoldenthal/pa11y@8.2.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.1.1`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.6.1, which includes updating to Chrome 130.
  - Updated to axe-core@4.10.2, which fixed several false positives.

## 10.1.0 (2024-10-21)

### Changed

- Updated to [`@aarongoldenthal/pa11y@8.2.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y)
  and [`@aarongoldenthal/pa11y-ci@4.1.0`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci),
  with the following significant changes:
  - Updated to puppeteer@23.6.0, which includes updating to Chrome 130.
  - Updated to axe-core@4.10.1, which fixed several false positives.

## 10.0.5 (2024-10-15)

### Fixed

- Updated to `serve@14.2.4`, resolving [CVE-2024-45296/GHSA-9wv6-86v2-598j](https://github.com/advisories/GHSA-9wv6-86v2-598j).

## 10.0.4 (2024-10-14)

### Fixed

- Updated to latest dependencies
  ([`@aarongoldenthal/pa11y@8.1.5`](https://www.npmjs.com/package/@aarongoldenthal/pa11y),
  [`@aarongoldenthal/pa11y-ci@4.0.4`](https://www.npmjs.com/package/@aarongoldenthal/pa11y-ci)).
  These forks are now published to npm, and include security updates to Chrome
  129, updating to Firefox 131, and resolving issue with `pa11y-ci` hanging
  when using a browser launched from the config file.

## 10.0.3 (2024-09-29)

### Fixed

- Updated to latest dependencies (`@aarongoldenthal/pa11y@8.1.4`,
  `@aarongoldenthal/pa11y-ci@4.0.3`). This includes security updates to
  `puppeteer` Chrome 129.
- Fixed CI pipeline to update OCI image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `puppeteer` image
  were cascading to this image.

## 10.0.2 (2024-09-21)

### Fixed

- Updated to latest dependencies (`@aarongoldenthal/pa11y@8.1.3`,
  `@aarongoldenthal/pa11y-ci@4.0.2`, `wait-on@8.0.1`). This includes
  updating `pa11y`/`pa11y-ci` to Chrome 129.

## 10.0.1 (2024-09-08)

### Fixed

- Updated to latest dependencies (`@aarongoldenthal/pa11y@8.1.2`,
  `@aarongoldenthal/pa11y-ci@4.0.1`).
- Fixed Renovate config to properly group `pa11y`/`pa11y-ci` dependencies.

### Miscellaneous

## 10.0.0 (2024-09-05)

### Changed

- BREAKING: Moved from `pa11y@8.0.0` to fork
  [`@aarongoldenthal/pa11y@8.1.0`](https://github.com/aarongoldenthal/pa11y/releases/tag/8.1.0)
  and from `pa11y-ci@3.1.0` to fork
  [`@aarongoldenthal/pa11y-ci@4.0.0`](https://github.com/aarongoldenthal/pa11y-ci/releases/tag/4.0.0).
  (#57)
- BREAKING: Updated base image to new [`puppeteer`](https://gitlab.com/gitlab-ci-utils/container-images/puppeteer),
  image based on Debian Bookworm Slim. (#58)
- Update to latest dependencies (`pa11y-ci-reporter-html@7.0.0`, `pa11y-ci-reporter-cli-summary@4.0.0`)

### Miscellaneous

- Updated pipeline to use
  [`Node-Security-Overrides`](https://gitlab.com/gitlab-ci-utils/gitlab-ci-templates/-/blob/33.8.0/templates/Node-Security-Overrides.gitlab-ci.yml)
  template.
- Updated pipeline to test `puppeteer` with the deprecated `headless: "shell"`
  since `headless: "new"` is now the default.
- Updated `renovate` configuration to group all `pa11y` dependency updates.

## 9.0.5 (2024-08-20)

### Fixed

- Updated to [`wait-on@8.0.0`](https://github.com/jeffbski/wait-on/releases/tag/v8.0.0).
  This change may be BREAKING if used to wait for a Unix socket connection,
  which is not expected for this project's use, so it was considered a patch
  release.

## 9.0.4 (2024-08-13)

### Fixed

- Refresh lockfile to resolve `axios` vulnerability
  [CVE-2024-39338](https://nvd.nist.gov/vuln/detail/CVE-2024-39338) /
  [GHSA-8hc4-vh64-cxmj](https://github.com/advisories/GHSA-8hc4-vh64-cxmj).

## 9.0.3 (2024-06-29)

### Fixed

- Updated `ws` package to resolve [CVE-2024-37890](https://osv.dev/vulnerability/GHSA-3h5v-q93c-6h6q).

### Miscellaneous

- Updated to Renovate config v1.1.0.

## 9.0.2 (2024-04-25)

### Fixed

- Updated to latest dependencies (`serve@14.2.3`)

## 9.0.1 (2024-04-22)

### Fixed

- Updated to latest dependencies (`serve@14.2.2`)

## 9.0.0 (2024-04-13)

### Changed

- BREAKING: Updated to `pa11y@8.0.0`, see the
  [release notes](https://github.com/pa11y/pa11y/releases/tag/8.0.0)
  for details.
  - Note that `pa11y-ci` has not yet been updated to this version of `pa11y`
    (it's still using v6.2.3).
- Update to [`pa11y-reporter-html-plus@4.0.1`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/4.0.1)

### Fixed

- Updated to Node 20.12.2, resolving [CVE-2024-27980](https://nodejs.org/en/blog/vulnerability/april-2024-security-releases-2#command-injection-via-args-parameter-of-child_processspawn-without-shell-option-enabled-on-windows-cve-2024-27980---high).

## 8.0.1 (2024-03-17)

### Fixed

- Update all dependencies, resolving [CVE-2024-28849](https://osv.dev/vulnerability/GHSA-cxjh-pqwp-8mfp).
- Removed `NODE_ENV=production` from image since primarily used for testing. (#56)

## 8.0.0 (2024-02-23)

### Changed

- BREAKING: Updated to `pa11y@7.0.0`, see the
  [release notes](https://github.com/pa11y/pa11y/releases/tag/7.0.0)
  for details.
  - Note that `pa11y-ci` has not yet been updated to this version of `pa11y`.
- Added `pa11y` test with `headless: "new"`, which will soon be the
  `puppeteer` default. See
  [this article](https://developer.chrome.com/docs/chromium/new-headless)
  for details.

### Fixed

- Updated image to specify `PUPPETEER_CACHE_DIR` (as
  `/home/pptruser/.cache/puppeteer`) to resolve `pa11y` launch issue with
  Puppeteer 20. (#54)
  - Note: `pa11y-ci` is using an older `puppeteer` version and did not have
    this issue.
- Updated pipeline to explicitly test `pa11y` since it uses a much different
  Chrome than `pa11y-ci`. Also resolves issues where job did not fail for
  some `pa11y-ci` failures. (#53)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#52)

## 7.4.0 (2023-11-14)

### Changed

- Updated image to Node 20, which is the active LTS release as of 2023-10-24.
  (#50)
- Updated to `pa11y-ci@3.1.0`, which updates to `pa11y@6.2.3`.

### Fixed

- Updated to `wait-on@7.2.0`, resolving CVE-2023-45857.

## 7.3.1 (2023-08-21)

### Fixed

- Updated to latest dependencies (`serve@14.2.1`)

## 7.3.0 (2023-08-14)

### Changed

- Updgraded to latest `pa11y` and `pa11y-ci` reporters:
  - [`pa11y-ci-reporter-html@6.0.2`](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/releases/6.0.2)
  - [`pa11y-reporter-html-plus@2.0.1`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/2.0.1)

## 7.2.0 (2023-06-11)

### Changed

- Updgraded to latest `pa11y` and `pa11y-ci` reporters:
  - [`pa11y-ci-reporter-html@6.0.0`](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/releases/6.0.0)
  - [`pa11y-ci-reporter-cli-summary@3.0.0`](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-cli-summary/-/releases/3.0.0)
  - [`pa11y-reporter-html-plus@2.0.0`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/2.0.0)

## 7.1.1 (2023-03-07)

### Changed

- Updgraded to
  [`pa11y-ci-reporter-html@5.1.1`](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/releases/5.1.1)
  and
  [`pa11y-reporter-html-plus@1.1.1`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/1.1.1).

## 7.1.0 (2023-03-01)

### Changed

- Updgraded to
  [`pa11y-ci-reporter-html@5.1.0`](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/releases/5.1.0)
  and
  [`pa11y-reporter-html-plus@1.1.0`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/1.1.0).

### Miscellaneous

- Added `lint_prettier` and `osv_scanner` jobs to CI pipeline. (#48)

## 7.0.3 (2023-02-15)

### Fixed

- Updated to latest dependencies (`pa11y-ci-reporter-html@5.0.4`, `pa11y-reporter-html-plus@1.0.5`, `serve@14.2.0`)

## 7.0.2 (2022-12-24)

### Fixed

- Updated to latest dependencies (`serve@14.1.2`, `wait-on@7.0.1`)

## 7.0.1 (2022-11-09)

### Fixed

- Updated to `serve@14.1.1` resolving [CVE-2022-3517](https://nvd.nist.gov/vuln/detail/CVE-2022-3517). (#45)
- Updated to `pa11y-ci-reporter-cli-summary@2.0.2`.

## 7.0.0 (2022-10-31)

### Changed

- BREAKING: Changed container image to Node 18, which the Active LTS as of 2022-10-25. The base image was also updated to be based on Node Debian Slim vs Node Debian, see full details [here](https://gitlab.com/gitlab-ci-utils/docker-puppeteer/-/blob/master/docs/debain-vs-debian-slim.md). (#46)
- Added standard set of `LABEL`s to images (documentation, license, etc). (#44)

### Miscellaneous

- Updated long running CI jobs to use GitLab shared `medium` runners. (#43)

## 6.1.1 (2022-09-25)

### Fixed

- Updated to latest dependencies (`pa11y-ci-reporter-html@5.0.1`)

## 6.1.0 (2022-08-20)

### Changed

- Added static web server [`serve`](https://www.npmjs.com/package/serve) and
  [`wait-on`](https://www.npmjs.com/package/wait-on) to wait until server has initialized. (#42)

### Fixed

- Updated to latest dependencies:
  - `pa11y-ci-reporter-cli-summary` to v2.0.1

## 6.0.0 (2022-07-19)

### Changed

- BREAKING: Updated `pa11y-ci-reporter-cli-summary` to v2.0.0
- BREAKING: Updated `pa11y-ci-reporter-html` to v5.0.1
- Added `pa11y-reporter-html-plus` (the new page reporter for `pa11y-ci-reporter-html` v5). (#41)

### Fixed

- Updated to latest dependencies

## 5.1.3 (2022-04-19)

### Fixed

- Updated to latest dependencies, including resolving CVE-2021-43138 and CVE-2021-44906 (dev only) (#38)
- Updated license name in `package.json` to reflect proper SPDX identifier (#39)

### Miscellaneous

- Add `prettier` for consistent formatting (#40)

## 5.1.2 (2022-01-26)

### Fixed

- Updated to latest dependencies, including resolving `node-fetch` vulnerability CVE-2022-0235

## 5.1.1 (2021-12-30)

### Fixed

- Updated to `pa11y-ci-reporter-cli-summary` v1.0.1 to fix several formatting issues

## 5.1.0 (2021-12-28)

### Added

- Added [`pa11y-ci-reporter-cli-summary`](https://www.npmjs.com/package/pa11y-ci-reporter-cli-summary) to the container image (#36)

### Fixed

- Update to latest [base image](https://gitlab.com/gitlab-ci-utils/docker-puppeteer)

## 5.0.0 (2021-12-20)

### Changed

- BREAKING: Updated `pa11y-ci` to v3.0.0 (see [migration guide](https://github.com/pa11y/pa11y-ci/blob/master/MIGRATION.md#migrating-from-20-to-30))
- BREAKING: Updated `pa11y-ci-reporter-html` to v4.0.0 (see [upgrade guide](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/blob/master/docs/upgrade-guide.md#upgrading-from-v3x-to-v4x))

### Fixed

- Update to latest [base image](https://gitlab.com/gitlab-ci-utils/docker-puppeteer)

## 4.1.2 (2021-11-17)

### Changed

- Updated to `pa11y` v6.1.1

## 4.1.1 (2021-11-12)

### Changed

- Updated to `pa11y-ci-reporter-html` v3.0.2

## 4.1.0 (2021-11-09)

### Changed

- Update to `pa11y` v6.1.0

## 4.0.0 (2021-10-29)

### Changed

- BREAKING: Update image to Node 16, which is now the active LTS release (#35)

### Miscellaneous

- Updated documentation on `--no-sandbox` with background on the cause, recommendations, and links to additional details. (#34)

## 3.1.0 (2021-10-09)

### Changed

- Move to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#33)

### Fixed

- Updated to latest dependencies

## 3.0.1 (2021-09-12)

### Fixed

- Updated to latest dependencies, including `pa11y-ci-reporter-html` v3.0.1

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates

## 3.0.0 (2021-08-17)

### Changes

- BREAKING: Update to `pa11y` v6, which defaults to WCAG 2.1 rules (note `pa11y-ci` still runs v5) (#30)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/noise-reduction/) for dependency updates (#29)

## 2.0.0 (2021-07-18)

### Changes

BREAKING: Changed license to [LGPL-3.0](<https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3)>) since the container image includes [pa11y-ci](https://github.com/pa11y/pa11y-ci) in the distribution which is LGPL-3.0, so the container must also be LGPL-3.0. (#28)

### Miscellaneous

- Updated pipeline to lint package-lock.json (#27)

## 1.6.9 (2021-07-04)

### Fixed

- Updated to latest `pa11y-ci` and `pa11y-ci-reporter-html`

### Miscellaneous

- Updated pipeline for GitLab 14 changes (#25) and to optimize Docker build by not downloading artifacts (#26)

## 1.6.8 (2021-06-06)

### Fixed

- Updated dependencies to resolve vulnerabilities

## 1.6.7 (2021-05-15)

### Fixed

- Updated to latest dependencies, including Pa11y CI HTML Reporter (v2.1.4)

## v1.6.6 (2021-05-02)

### Fixed

- Updated to latest pa11y-ci and other dependencies

## v1.6.5 (2021-04-03)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to use `needs` for efficiency (#23)

## v1.6.4 (2021-03-27)

### Fixed

- Updated to latest dependencies

## v1.6.3 (2021-03-13)

### Fixed

- Updated to latest dependencies

## v1.6.2 (2021-03-07)

### Fixed

- Updated to latest Pa11y CI HTML Reporter (v2.1.2)
- Updated to latest dependencies to resolve vulnerabilities

### Miscellaneous

- Updated CI pipeline to use GitLab Releaser

## v1.6.1 (2020-11-29)

### Changed

- Updated to latest Pa11y CI HTML Reporter (v2.1.1)

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#21)

## v1.6.0 (2020-10-27)

### Changed

- Updated to latest Node LTS release (v14)

## v1.5.0 (2020-10-19)

### Changed

- Updated to latest Pa11y CI HTML Reporter

## v1.4.3 (2020-09-20)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.4.2 (2020-08-23)

### Fixed

- Updated to latest `pa11y-ci`

## v1.4.1 (2020-08-09)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.4.0 (2020-07-06)

### Changed

- Updated to latest `axe-core`
- Simplified logic in container self-test CI job (#20)

## v1.3.0 (2020-04-21)

### Changed

- Added all `pa11y`-related dependencies explicitly to trigger notification of updates
- Updated to latest `html_codesniffer`

## v1.2.2 (2020-04-05)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.2.1 (2020-03-22)

### Fixed

- Updated to latest Pa11y CI HTML Reporter

## v1.2.0 (2020-01-22)

### Changed

- Updated to latest Pa11y CI HTML Reporter

## v1.1.3 (2019-11-01)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.1.2 (2019-11-17)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.1.1 (2019-10-20)

### Fixed

- Updated to use latest dependencies

## v1.1.0 (2019-10-09)

### Changed

- Updated to use [Docker Puppeteer](https://gitlab.com/gitlab-ci-utils/docker-puppeteer) as the base image (#13)
- Added pa11y as a dedicated dependency so it is checked in every CI pipeline and updated to the latest version (#14)

## v1.0.2 (2019-08-04)

### Changed

- Updated to latest version of Pa11y CI HTML Reporter (1.2.0)

## v1.0.1 (2019-07-14)

### Changed

- Updated to latest version of Pa11y CI HTML Reporter

## v1.0.0 (2019-06-16)

### Changed

- Updated test-pa11y-ci job to fail if pa11y-ci exits for any reason except accessibility errors (#10)
- Updated to latest version of Pa11y CI HTML Reporter

### Documentation

- Added details on pa11y-ci-reporter-html (#9)
- Moved repository to a new group and updated documentation

## 0.6.0 (2019-06-03)

### Added

- Added `pa11y-ci-reporter-html` module to allow HTML reports, see <https://www.npmjs.com/package/pa11y-ci-reporter-html>.

### Documentation

- Clarified requirements for Chrome `--no-sandbox` flag, which is only required when running this Linux container under Docker for Windows (not Linux).

## 0.5.1 (2019-05-05)

### Changed

- Update `PATH` so `pa11y-ci` can be called directly.

## 0.5.0 (2019-05-03)

### Initial Release

- Initial container image with pa11y-ci capability and implementation details.
