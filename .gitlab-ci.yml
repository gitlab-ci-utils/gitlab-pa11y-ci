include:
  - project: 'gitlab-ci-utils/gitlab-ci-templates'
    ref: 38.1.2
    file:
      - '/collections/All-Projects.gitlab-ci.yml'
      - '/collections/Container-Build-Test-Deploy.gitlab-ci.yml'
      - '/collections/GitLab-Security-Scans-Schedules-Fail.gitlab-ci.yml'
      - '/jobs/Lint-Npm-Lockfile.gitlab-ci.yml'
      - '/jobs/Npm-Install.gitlab-ci.yml'
      - '/jobs/Npm-Lint-Prettier.gitlab-ci.yml'
      - '/jobs/Npm-Check-Schedules-Fail.gitlab-ci.yml'
      - '/templates/Container-Annotations-Overrides.gitlab-ci.yml'
      - '/templates/Node-Security-Overrides.gitlab-ci.yml'

# Included from Npm-Install.gitlab-ci.yml
npm_install:
  variables:
    # Override for all puppeteer install and execution jobs
    PUPPETEER_CACHE_DIR: $CI_PROJECT_DIR/node_modules/.cache/puppeteer

# Disable SAST per #19
variables:
  SAST_DISABLED: 'true'

# Included from Lint-Prettier.gitlab-ci.yml
lint_prettier:
  needs:
    - npm_install

# Included from Lint-Md.gitlab-ci.yml
lint_md:
  script:
    - markdownlint-cli2 "**/*.md" "#node_modules"

# Included from Container-Build-Test-Deploy.gitlab-ci.yml
container_build:
  # Use medium runners to reduce pipeline time (since in critical path)
  tags:
    - saas-linux-medium-amd64

# Included from Container-Build-Test-Deploy.gitlab-ci.yml
# Use medium runners to reduce pipeline time (since in critical path)
container_scanning:
  tags:
    - saas-linux-medium-amd64

test_pa11y_ci:
  image: registry.gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/tmp:${CI_PIPELINE_ID}
  stage: container-test
  needs:
    - container_build
  script:
    # Expect this test to fail since it's scanning sites with known a11y issues.
    # So, this check the results and fails as appropriate.  If pa11y-ci passes
    # then exit 2. If it fails, then check the exit code ($?). Exit code 1 is a
    # pa11y-ci execution error, so if that occurs exit 1. If exit code 2
    # (accessibility error, which is expected), then exit 0.
    - pa11y-ci && exit 2 || if [ "$?" -eq "1" ]; then exit 1; else exit 0; fi
  artifacts:
    when: always
    paths:
      - pa11y/
    reports:
      accessibility: pa11y/pa11y-ci-results.json

test_pa11y:
  image: registry.gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/tmp:${CI_PIPELINE_ID}
  stage: container-test
  needs:
    - container_build
  before_script:
    # Start static server and wait until running
    - serve ./public/ > /dev/null 2>&1 & wait-on http://localhost:3000/
  script:
    # Expect this test to pass since no a11y issues
    - pa11y http://localhost:3000/

# Repeat pa11y test with headless: shell (old headless)
test_pa11y_headless_shell:
  extends:
    - test_pa11y
  script:
    # Expect this test to pass since no a11y issues
    - pa11y -c .pa11y.json http://localhost:3000/

# Included from Container-Build-Test-Deploy.gitlab-ci.yml
.copy_image:
  needs:
    - container_scanning
    - container_dive
    - test_pa11y_ci
    - test_pa11y
    - test_pa11y_headless_shell
    - update_annotations

# Included from All-Projects.gitlab-ci.yml
# Update needs to optimize pipeline.
prepare_release:
  needs: []

# Included from All-Projects.gitlab-ci.yml
# Update needs to optimize pipeline.
create_release:
  needs:
    - prepare_release
    - deploy_tag
